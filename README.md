###################SPI CONNECTION##################
ARDUINO UNO (OLED)
OLED    ARDUINO UNO
GND      GND
VCC       5v
SCL       13 (SCK)
SDA       11 (MOSI)
RES        8
DC         9
CS         10 (SS)

https://forum.arduino.cc/index.php?topic=442255.0  

==========================================================

HX1230 Arduino Nano

U8g2 Library
GND - GND
BL - 3V/5V
VCC - 3V/5V
CLK - 13
DIN - 11
N/C - 9
CE - 10
RST - 8

==========================================================

STM32 	

// If using software SPI (the default case):
#define OLED_MOSI  PB7//9 //SDA -> D9(PB1)
#define OLED_CLK   PB8//10 //SCL -> D10(PB2)
#define OLED_DC    PA6//11 //DC -> D11(PB3)
#define OLED_CS    PB12//PB3//12 // Not connected
#define OLED_RESET PA7//13 //RST -> D13(PB5)  

https://forum.arduino.cc/index.php?topic=566057.0  

==========================================================

HX1230 STM32 Connections to LCD using u8g2 library
#1 RST - PA0 
#2 CS/CE - PA4 
#3 DC - PA1 
#4 MOSI/DIN - PA7 
#5 SCK/CLK - PA5 
#6 VCC - 3.3V 
#7 LIGHT - 200ohm to GND 
#8 GND 

https://www.youtube.com/watch?v=Vtd0p4xMSGI 

========================================================
ESP32 SPI CONNECTION

https://www.az-delivery.de/it/blogs/azdelivery-blog-fur-arduino-und-raspberry-pi/1-8-toll-tft-am-esp-32-dev-kit-c-betreiben 


========================================================

ESP8266 SPI CONNECTION(HX1230 Display)
PCD8544/LED -> ESP8266
Pin 1 - RST -> D0/GPIO16
Pin 2 - CE (CS) -> D8/GPIO15 - pull down with 10k resistor to GND
Pin 3 - DC -> D4/GPIO2
Pin 4 - DIN -> D7/HMOSI (fixed - you can't use a different pin)
Pin 5 - CLK -> D5/HSCLK (fixed - you can't use a different pin)
Pin 6 - VCC -> 3.3V - either from Development board or other sources
Pin 7 - BL .... Not used in this example
Pin 8 - GND -> GND - either from Development board or another source.


U8G2_HX1230_96X68_2_3W_SW_SPI u8g2(U8G2_R0, /* clock=*/ D5, /* data=*/ D7, /* cs=*/ D8 ,/* reset=*/ D0);

https://www.hackster.io/sotm/nokia-5110-pcd8544-with-nodemcu-esp8266-using-lua-and-u8g2-8ea055

